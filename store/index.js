import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

let lifeData = {};

try {
	// 尝试获取本地是否存在lifeData变量，第一次启动APP时是不存在的
	lifeData = uni.getStorageSync('lifeData');
} catch (e) {

}

// 需要永久存储，且下次APP启动需要取出的，在state中的变量名
let saveStateKeys = ['vuex_user', 'vuex_token', "vuex_loginStatus", "vuex_userInfo","vuex_registerStatus"];

// 保存变量到本地存储中
const saveLifeData = function(key, value) {
	// 判断变量名是否在需要存储的数组中
	if (saveStateKeys.indexOf(key) != -1) {
		// 获取本地存储的lifeData对象，将变量添加到对象中
		let tmp = uni.getStorageSync('lifeData');
		// 第一次打开APP，不存在lifeData变量，故放一个{}空对象
		tmp = tmp ? tmp : {};
		tmp[key] = value;
		// 执行这一步后，所有需要存储的变量，都挂载在本地的lifeData对象中
		uni.setStorageSync('lifeData', tmp);
	}
}
const store = new Vuex.Store({
	state: {
		// 如果上面从本地获取的lifeData对象下有对应的属性，就赋值给state中对应的变量
		// 加上vuex_前缀，是防止变量名冲突，也让人一目了然
		vuex_user: lifeData.vuex_user ? lifeData.vuex_user : '登录以探索更多',
		vuex_token: lifeData.vuex_token ? lifeData.vuex_token : '',
		// 新添加的登录状态 ->  0: 未登录, 1: 已登录
		vuex_loginStatus: lifeData.vuex_loginStatus ? lifeData.vuex_loginStatus : 0,
		vuex_registerStatus: lifeData.vuex_registerStatus ? lifeData.vuex_registerStatus : false,
		vuex_userInfo: lifeData.vuex_userInfo ? lifeData.vuex_userInfo : {
			sex: 0,
			imageUrl: '',
			school: '',
			phone: '',
			grade: '',
			userId: '',
		},
		vuex_taskInfo:{
			total: 0,
			publishCount: 0,
			workerCount: 0
		},
		// 如果vuex_version无需保存到本地永久存储，无需lifeData.vuex_version方式
		vuex_version: '1.0.1',
		vuex_demo: '绛紫',
		// 自定义tabbar数据
		vuex_tabbar: [{
				iconPath: "/static/carry-inner-icon/bottom-tab-icon/home_normal.png",
				selectedIconPath: "/static/carry-inner-icon/bottom-tab-icon/home_fire.png",
				text: '首页',
				pagePath: '/pages/tabPages/index'
			},
			/* {
				iconPath: "/static/uview/example/js.png",
				selectedIconPath: "/static/uview/example/js_select.png",
				text: '动态',
				pagePath: '/pages/example/js'
			}, */
			{
				iconPath: "/static/carry-inner-icon/bottom-tab-icon/add.png",
				selectedIconPath: "/static/carry-inner-icon/bottom-tab-icon/add.png",
				text: '发布',
				midButton: true,
				pagePath: '/pages/tabPages/add'
			},
			/* {
				iconPath: "/static/uview/example/js.png",
				selectedIconPath: "/static/uview/example/js_select.png",
				text: '消息',
				pagePath: '/pages/example/template'
			}, */
			{
				iconPath: "/static/carry-inner-icon/bottom-tab-icon/my_normal.png",
				selectedIconPath: "/static/carry-inner-icon/bottom-tab-icon/my_fire.png",
				text: '我的',
				pagePath: '/pages/tabPages/my'
			}
		],
		vuex_onTabChange(index) {
			console.log('当前' + index + '被单击')
		},
		/**
		 * 臧锡洋
		 * 2020年11月24日 02:26:13
		 * 清楚登录信息
		 */
		vuex_clearLoginInfo() {
			// 设置登录状态为未登录
			this.$u.vuex('vuex_loginStatus', 0)
			this.$u.vuex('vuex_token', '')
			this.$u.vuex('vuex_userInfo', {})
			this.$u.vuex('vuex_token', '')
			this.$u.vuex('vuex_user',"登录以探索更多")
		}
	},
	mutations: {
		$uStore(state, payload) {
			// 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
			let nameArr = payload.name.split('.');
			let saveKey = '';
			let len = nameArr.length;
			if (len >= 2) {
				let obj = state[nameArr[0]];
				for (let i = 1; i < len - 1; i++) {
					obj = obj[nameArr[i]];
				}
				obj[nameArr[len - 1]] = payload.value;
				saveKey = nameArr[0];
			} else {
				// 单层级变量，在state就是一个普通变量的情况
				state[payload.name] = payload.value;
				saveKey = payload.name;
			}
			// 保存变量到本地，见顶部函数定义
			saveLifeData(saveKey, state[saveKey])
		}
	}
})

export default store
