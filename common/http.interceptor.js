// 这里的vm，就是我们在vue文件里面的this，所以我们能在这里获取vuex的变量，比如存放在里面的token
// 同时，我们也可以在此使用getApp().globalData，如果你把token放在getApp().globalData的话，也是可以使用的
const install = (Vue, vm) => {
	Vue.prototype.$u.http.setConfig({
		baseUrl: 'https://bd.imsle.com',
		// 如果将此值设置为true，拦截回调中将会返回服务端返回的所有数据response，而不是response.data
		// 设置为true后，就需要在this.$u.http.interceptor.response进行多一次的判断，请打印查看具体值
		// originalData: true, 
		// 设置自定义头部content-type
		header: {
			'content-type': 'application/json;charset=UTF-8'
		}
	});
	// 请求拦截，配置Token等参数
	Vue.prototype.$u.http.interceptor.request = (config) => {
		config.header.Authorization = vm.vuex_token;

		// 对登录鉴权接口进行拦截,不添加请求头
		if (config.url.search('/grant/loginByCode') != -1 || config.url == '/grant/register') {
			// 通过loginCode登录,注册新账户时不携带header请求头
			config.header.Authorization = ''
		}
		console.log(config.data)

		// 方式一，存放在vuex的token，假设使用了uView封装的vuex方式，见：https://uviewui.com/components/globalVariable.html
		// config.header.token = vm.token;

		// 方式二，如果没有使用uView封装的vuex方法，那么需要使用$store.state获取
		// config.header.token = vm.$store.state.token;

		// 方式三，如果token放在了globalData，通过getApp().globalData获取
		// config.header.token = getApp().globalData.username;

		// 方式四，如果token放在了Storage本地存储中，拦截是每次请求都执行的，所以哪怕您重新登录修改了Storage，下一次的请求将会是最新值
		// const token = uni.getStorageSync('token');
		// config.header.token = token;

		return config;
	}
	// 响应拦截，判断状态码是否通过
	Vue.prototype.$u.http.interceptor.response = (res) => {
		// 如果把originalData设置为了true，这里得到将会是服务器返回的所有的原始数据
		// 判断可能变成了res.statueCode，或者res.data.code之类的，请打印查看结果
		/* if (res.code == 1000) {
			// 如果把originalData设置为了true，这里return回什么，this.$u.post的then回调中就会得到什么
			return res.data;
		} else if (res.code == 1001) {
			console.log("权限过期重新登录 TODO 显示界面 ->" + res.message)
			// 清除当前vuex中的loginStatus和 loginToken
			vm.$u.vuex('vuex_token', '')
			vm.$u.vuex('vuex_loginStatus',0)
			vm.$u.vuex('vuex_user','未登录用户')
			return false;
		} else return false; */
		/* if (res.code == 1002){
			console.log("当前无权限需要重新登录")
			// 清除权限信息
			vm.vuex_clearLoginInfo()
			return false
		} */
		if (res.code == 1001) {
			// 服务端出错
			// TODO 全局处理
		} else if (res.code == 1002) {
			// 需要登录
			//TODO 本地缓存全部清掉
			vm.vuex_clearLoginInfo()
			uni.showModal({
				title: "您当前没有登录",
				content: '缺少必要的权限,请先登录',
				confirmText: '了解',
				showCancel: false,
				success: (res) => {
					if (res.confirm) {
						uni.switchTab({
							url: '/pages/tabPages/my'
						});
					}
				}
			})
			return false
		} else return res
	}
}

export default {
	install
}
