// 如果没有通过拦截器配置域名的话，可以在这里写上完整的URL(加上域名部分)
let bannerUrl = '/advert/banner';
let noticeUrl = '/advert/notice';
const grantUrl = '/grant/'; // 授权地址
const orderUrl = '/order/'; // 订单地址
const profileUrl = '/profile' //个人信息



// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
// https://uviewui.com/js/http.html#%E4%BD%95%E8%B0%93%E8%AF%B7%E6%B1%82%E6%8B%A6%E6%88%AA%EF%BC%9F
const install = (Vue, vm) => {
	// 此处没有使用传入的params参数

	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月23日 01:47:42
	 * 说明: 请求首页banner
	 */
	let getBanner = () => vm.$u.get(bannerUrl);

	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月23日 01:47:42
	 * 说明: 请求首页通知
	 */
	let getNotice = () => vm.$u.get(noticeUrl);


	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月23日 02:54:18
	 * 说明: 通过loginCode进行登录,详见swagger文档
	 */
	let loginByCode = (code) => vm.$u.get(`${grantUrl}loginByCode?code=${code}`)


	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月23日 02:54:18
	 * 说明: 注册接口,详见swagger文档
	 *  "dy": "string",
	 *	"iv": "string",
	 *  "openId": "string"
	 */
	let register = (param = {}) => vm.$u.post(`${grantUrl}register`, JSON.stringify(param))
	
	
	/* 登出 */
	let logout = () => vm.$u.post(`${grantUrl}logout`)
	
	
	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月23日 15:14:33
	 * 说明: 测试鉴权接口
	 */

	let testAuth = () => vm.$u.get(`${grantUrl}testAuth`)


	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月23日 15:14:33
	 * 说明: 修改用户信息接口
	 * 	"grade": "string",
	 *	"nikeName": "string",
	 *	"phone": "string",
	 *	"school": "string",
	 *	"sex": Number
	 */
	let profile = (param = {}) => vm.$u.put(profileUrl, JSON.stringify(param))


	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月23日 15:14:33
	 * 说明: 发布任务接口
	 * 	moneyReward	number(double) 赏金
	 * 
	 *	pickUpAddress	string 取件地址
	 *	
	 *	publishName	string 发布人姓名
	 *	
	 *	publishPhone	string 发布者电话
	 *	
	 *	taskTitle	string 任务标题
	 *	
	 *	taskType	int 任务类型 1 取快递 2 跑腿
	 *	
	 *	toAddress	string 送达地址
	 *	
	 *	workContent	string 任务描述
	 */
	let publishTask = (param = {}) => vm.$u.post('/publish/task', JSON.stringify(param))


	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月24日 05:19:24
	 * 说明: 获取待接收的任务
	 */
	let getTasks = (pageNum = 1, pageSize = 15, queryStr = '') => vm.$u.get(
		`/task/pending?pageNum=${pageNum}&pageSize=${pageSize}&queryStr=${queryStr}`)

	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月24日 06:24:37
	 * 说明: 通过任务id来获取当前任务详情
	 */

	let getTaskDetailById = (id) => vm.$u.get(`/task/pending/${id}`)

	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月24日 07:29:42
	 * 说明: 通过任务ID来接取订单
	 */
	let acceptTask = (id) => vm.$u.post(`/worker/task/accept/${id}`)
	
	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月24日 07:29:42
	 * 说明: 任务中心
	 */
	let taskCenter = (pageNum = 1, pageSize = 15, queryStr = '') => vm.$u.get(`/task/center?pageNum=${pageNum}&pageSize=${pageSize}&queryStr=${queryStr}`)

	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月24日 08:24:30
	 * 说明: 发布任务用户取消任务
	 */
	let cancelTask = (param={}) => vm.$u.put('/publish/task/cancel',JSON.stringify(param))
	
	/**
	 * 作者: 臧锡洋
	 * 时间: 2020年11月24日 08:24:30
	 * 说明: 接收任务用户取消任务
	 */
	let cancelTaskByWorker = (param = {}) => vm.$u.put('/worker/task/cancel',JSON.stringify(param))
	
	
	/* 确认完成 */
	let confirmTask =  (id) => vm.$u.put(`/publish/task/complete/${id}`)
	
	// let acceptOrder = 
	
	/**
	 * 作者: 谢广顺
	 * 时间: 2020年11月24日 12:54:33
	 * 说明: 反馈信息接口
	 *  content string 反馈内容
	 * 
	 *	type	int 反馈信息
	 *	
	 */
	let publishReturns = (param = {}) => vm.$u.post('/profile/feedback', JSON.stringify(param))
	
	/**
	 * 作者: 谢广顺
	 * 时间: 2020年11月24日 13:54:33
	 * 说明: 更新个人信息接口
	 * grade: "string", 
	 * nikeName: "string",
	 * phone: "string",
	 * school: "string",
	 * sex: 1
	 * 最后修改 臧锡洋
	 * 版本 V2 -> 修改 POST -> PUT
	 */
	let updateinfo = (param = {}) => vm.$u.put('/profile', JSON.stringify(param))
	
	/**
	 * 作者: 谢广顺
	 * 时间: 2020年11月24日 14:24:37
	 * 说明: 通过反馈id来获取当前反馈详情
	 */
	
	let getReturnDetailById = (id) => vm.$u.get(`/profile/feedback/${id}`)
	
	/**
	 * 作者: 谢广顺
	 * 时间: 2020年11月24日 14:29:42
	 * 说明: 获取用户反馈列表
	 */
	let getReturnList = (pageNum = 1, pageSize = 15) => vm.$u.get(`/profile/feedback?pageNum=${pageNum}&pageSize=${pageSize}`)
	
	/**
	 * 作者: 谢广顺
	 * 时间: 2020年11月24日 14:54:33
	 * 说明: 用户确认问题解决状态
	 * id: int, 
	 * solveStatus: int,
	 *  修改: 臧锡洋 将 POST修改PUT
	 */
	let confirmResolveStatus = (param = {}) => vm.$u.put('/profile/feedback/confirm', JSON.stringify(param))
	
	
	/**
	 * 获取当前任务统计
	 */
	let getTaskCount = ()=> vm.$u.get('/task/count')
	
	
	
	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		getBanner,
		getNotice,
		loginByCode,
		register,
		testAuth,
		profile,
		publishTask,
		getTasks,
		getTaskDetailById,
		acceptTask,
		taskCenter,
		cancelTask,
		cancelTaskByWorker,
		confirmTask,
		publishReturns,
		updateinfo,
		getReturnDetailById,
		getReturnList,
		confirmResolveStatus,
		getTaskCount,
		logout
	};
}

export default {
	install
}
